# H6Find

Program to find a 6-Layer homogenous field in a set of POIs. This refers to the mobile game
ingress. Further information can be found in this great video series about [Homogenous fielding](https://www.youtube.com/watch?v=OvxD-iMgVPM)
by Michael Hartley.

## Installation

Linux: A simple make compiles the sources and puts the executable file H6Find in the build folder. That's it!

## Usage example

TODO

