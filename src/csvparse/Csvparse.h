#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "../balltree/Point.h"

namespace csv_parse{
    class CsvParse{
        public:
            static std::vector<ball_tree::Point>  parseInputFile(std::string inFile);

        private:
            static ball_tree::Point parseLine(std::string line); 

            static std::vector<std::string> splitString(std::string s);   
    };
}