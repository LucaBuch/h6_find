#include "Csvparse.h"

namespace csv_parse
{
    std::vector<ball_tree::Point> CsvParse::parseInputFile(std::string inFile)
    {
        std::ifstream inStream(inFile);
        std::vector<ball_tree::Point> points;
        if (inStream.is_open())
        {
            std::string line;
            while (std::getline(inStream, line))
            {
                ball_tree::Point p = parseLine(line);
                if (line == "")
                {
                    inStream.clear();
                    continue;
                }
                points.push_back(p);
                line = "";
            }
        }
        inStream.close();
        std::cout << points.size();
        return points;
    };

    ball_tree::Point CsvParse::parseLine(std::string input)
    {
        std::vector<std::string> tokens = splitString(input);
        double yCoord = std::stod(tokens[1]);
        double xCoord = std::stod(tokens[2]);
        ball_tree::Point p(xCoord, yCoord, tokens[0]);
        return p;
    };

    std::vector<std::string> CsvParse::splitString(std::string s)
    {
        std::vector<std::string> splitted;
        bool flag = false;
        splitted.push_back("");
        for (int i = 0; i < s.size(); ++i)
        {
            if (s[i] == '\"')
            {
                flag = flag ? false : true;
                continue;
            }

            if (s[i] == ',' && !flag)
                splitted.push_back("");
            else
                splitted[splitted.size() - 1] += s[i];
        }
        return splitted;
    };
} // namespace csv_parse