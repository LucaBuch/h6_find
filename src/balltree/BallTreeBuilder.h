#pragma once

#include "BallTree.h"
#include "Point.h"
#include <vector>
#include <utility>
#include <limits>
#include <algorithm>

namespace ball_tree
{

    enum class Dimension
    {
        X,
        Y
    };
    enum class PivotState
    {
        UNSELECTED,
        SELECTED_1,
        SELECTED_2
    };

    class BallTreeBuilder
    {
    public:
        /**
         * Static method to construct a BallTree from a given vector of points
         */
        static BallTree *constructBallTree(std::vector<Point> points);

    private:
        /**
         * Calculated the center point and the split dimension of a given
         * vector of points.
         * Return value 'X' equals the x axis, 'Y' equals the y axis
         */
        static std::pair<std::vector<Point>, Dimension> calculateSplit(std::vector<Point> points);
    };
}; // namespace ball_tree