#pragma once

#include <vector>
#include <utility>
#include <iostream>
#include <string>
#include "Point.h"
#include <iterator>
#include <stack>

namespace ball_tree
{

    class BallTree
    {
    public:
        class BallTreeIterator
        {
        public:
            typedef BallTreeIterator self_type;
            typedef Point value_type;
            typedef Point &reference;
            typedef Point *pointer;
            typedef std::forward_iterator_tag iterator_category;
            typedef std::ptrdiff_t difference_type;

            BallTreeIterator(BallTree *start);

            BallTreeIterator operator++();

            BallTree *operator*();

            bool operator!=(BallTreeIterator &iter);

        private:
            std::stack<BallTree *> nodes;
        };
        /**
         * Constructor for a single BallTree node. This method does not consruct
         * a full BallTree. For doing so use the BallTreeBuilder
         */
        BallTree(Point center, BallTree *rightChild, BallTree *leftChild, double diameter);

        BallTree(Point center, double diameter);

        ~BallTree();

        Point getPoint();

        double getDiameter();

        std::pair<BallTree *, BallTree *> getChildren();

        std::string print();

    private:
        // The subballs of the node
        std::pair<BallTree *, BallTree *> children;

        std::string print(int indent);

        //The center point of the node
        Point center;

        double diameter;
    };
}; // namespace ball_tree
