#include "BallTree.h"

namespace ball_tree
{
	BallTree::BallTree(Point center, BallTree *rightChild, BallTree *leftChild, double diameter) : center(center), diameter(std::abs(diameter))
	{
		children.first = rightChild;
		children.second = leftChild;
	};

	BallTree::BallTree(Point center, double diameter) : center(center), diameter(std::abs(diameter))
	{
		children.first = NULL;
		children.second = NULL;
	};

	BallTree::~BallTree()
	{
		if (children.first != NULL)
		{
			delete children.first;
		}
		if (children.second != NULL)
		{
			delete children.second;
		}
	}

	Point BallTree::getPoint()
	{
		return center;
	}

	double BallTree::getDiameter(){
		return diameter;
	}

	std::pair<BallTree *, BallTree *> BallTree::getChildren()
	{
		return children;
	}

	std::string BallTree::print()
	{
		return print(1);
	}

	std::string BallTree::print(int indent)
	{
		std::string current(std::to_string(center.getX()) + "," + std::to_string(center.getY()));
		std::string indent_str("");
		for (int i = 0; i < indent; i++)
		{
			indent_str += "    |";
		}
		if (children.first != NULL)
		{
			current += "\n" + indent_str + children.first->print(indent + 1);
		}
		if (children.second != NULL)
		{
			current += "\n" + indent_str + children.second->print(indent + 1);
		}
		return current;
	}

	typedef BallTree::BallTreeIterator BallTreeIterator;

	BallTreeIterator::BallTreeIterator(BallTree *start)
    {
        nodes.push(start);
    }

    BallTreeIterator BallTreeIterator::operator++()
    {
        BallTree* current = nodes.top();
        nodes.pop();
        if(current->getChildren().first != NULL){
            nodes.push(current->getChildren().first);
        }
        if(current->getChildren().second != NULL){
            nodes.push(current->getChildren().second);
        }
        return *this;
    }

    BallTree *BallTreeIterator::operator*(){
        if(nodes.size() == 0){
            return nullptr;
        }
        return nodes.top();
    }

    bool BallTreeIterator::operator!=(BallTreeIterator& iter){
        return *iter == nodes.top();
    }
}; // namespace ball_tree
