#pragma once

#include <string>

namespace ball_tree
{
    class Point
    {
    public:
        Point(double x_coord, double y_coord) : x(x_coord), y(y_coord), name(""){};
        Point(double x_coord, double y_coord, std::string name) : x(x_coord), y(y_coord), name(name){};

        double getX();
        double getY();
        std::string getName();

    private:
        double x;
        double y;
        std::string name;
    };
}; // namespace ball_tree