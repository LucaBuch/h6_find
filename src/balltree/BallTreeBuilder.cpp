#include "BallTreeBuilder.h"

namespace ball_tree
{
    BallTree *BallTreeBuilder::constructBallTree(std::vector<Point> points)
    {
        if (points.size() == 1)
        {
            return new BallTree(points[0],0);
        }

        if(points.size() == 2){
            std::vector<Point> points_1 = {points[0]};
            BallTree *leftB = BallTreeBuilder::constructBallTree(points_1);
            double radius = std::max(std::abs(points[0].getX() - points[1].getX()), std::abs(points[0].getY() - points[1].getY()));
            return new BallTree(points[1], nullptr, leftB, radius);
        }

        //Non trivial case more than 2 points
        std::pair<std::vector<Point>, ball_tree::Dimension> split = calculateSplit(points);
        int pivot = split.first.size() / 2;
        std::vector<Point> left(split.first.begin(),split.first.begin() + pivot);
        std::vector<Point> right(split.first.begin() + pivot + 1,split.first.begin() + split.first.size());
        if(split.second == Dimension::X){
            return new BallTree(split.first[pivot], BallTreeBuilder::constructBallTree(left), BallTreeBuilder::constructBallTree(right),split.first[0].getX() - split.first[split.first.size() - 1].getX());
        }
        else{
            return new BallTree(split.first[pivot], BallTreeBuilder::constructBallTree(left), BallTreeBuilder::constructBallTree(right),split.first[0].getY() - split.first[split.first.size() - 1].getY());
        }
    };

    std::pair<std::vector<Point>, Dimension> BallTreeBuilder::calculateSplit(std::vector<Point> points)
    {
        //Calculate the dimension of biggest spread
        double small = std::numeric_limits<double>::max();
        double big = std::numeric_limits<double>::min();
        double xDiff = 0;
        double yDiff = 0;
        for (int i = 0; i < points.size(); i++)
        {
            if (points[i].getX() < small)
            {
                small = points[i].getX();
            }
            if (points[i].getX() > big)
            {
                big = points[i].getX();
            }
        }
        xDiff = big - small;
        small = std::numeric_limits<double>::max();
        big = std::numeric_limits<double>::min();
        for (int i = 0; i < points.size(); i++)
        {
            if (points[i].getY() < small)
            {
                small = points[i].getY();
            }
            if (points[i].getY() > big)
            {
                big = points[i].getY();
            }
        }
        yDiff = big - small;

        //Check for bigger dimension, sort and return middle point and dimension
        if (yDiff > xDiff)
        {
            std::sort(points.begin(), points.end(), [](Point &a, Point &b) -> bool {
                return a.getY() > b.getY();
            });
            return std::pair<std::vector<Point>, Dimension>(points, Dimension::Y);
        }
        else
        {
            std::sort(points.begin(), points.end(), [](Point &a, Point &b) -> bool {
                return a.getX() > b.getX();
            });
            return std::pair<std::vector<Point>, Dimension>(points, Dimension::X);
        }
    };
}; // namespace ball_tree