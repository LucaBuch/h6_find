#include <iostream>
#include <chrono>
#include "balltree/Point.h"
#include "balltree/BallTree.h"
#include "balltree/BallTreeBuilder.h"
#include "csvparse/Csvparse.h"

class Main
{
public:
	int main(int argc, const char *argv[]);
};
