#include "H6Find.h"
#include <vector>

int main(int argc, const char *argv[])
{
	std::cout << "Hello, from H6Find!\n";
	ball_tree::Point p1(0, 0);
	std::cout << p1.getX() << p1.getY() << std::endl;
	std::vector<ball_tree::Point> portals = csv_parse::CsvParse::parseInputFile("Portal_Export.csv");
	ball_tree::BallTree *bt = ball_tree::BallTreeBuilder::constructBallTree(portals);
	std::cout << bt->print() << std::endl;
	ball_tree::BallTree::BallTreeIterator iter = ball_tree::BallTree::BallTreeIterator(bt);
	ball_tree::BallTree *start = *iter;
	std::cout << "\n"
			  << start->getPoint().getName();
	while (*iter != nullptr)
	{
		start = *iter;
		std::cout << "\n"
				  << start->getPoint().getName();
		++iter;
	}
	delete bt;
}
