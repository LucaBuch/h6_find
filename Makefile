BUILD_DIR		:= build/
OBJECT_FILES	:= $(BUILD_DIR)H6Find.o $(BUILD_DIR)balltree/BallTree.o $(BUILD_DIR)balltree/Point.o $(BUILD_DIR)balltree/BallTreeBuilder.o $(BUILD_DIR)balltree/BallTreeCalculator.o $(BUILD_DIR)csvparse/Csvparse.o
CPP_TEST_FILES 	:= $(wildcard test/*.cpp test/*/*.cpp)
CPP_FILES		:= $(wildcard src/*/*.cpp)
CXX_FLAGS		:= -g -DDEBUG -std=c++20 -o

.PHONY: clean, test, debug

build: $(OBJECT_FILES) #Add specialized rules later on, working for now
	$(CXX) $(CXX_FLAGS) $(BUILD_DIR)H6Find $^
	chmod +x $(BUILD_DIR)H6Find

$(BUILD_DIR)H6Find.o: src/H6Find.cpp
	mkdir -p  $(BUILD_DIR)
	$(CXX) -c src/H6Find.cpp $(CXX_FLAGS) $(BUILD_DIR)H6Find.o

$(BUILD_DIR)balltree/%.o: src/balltree/%.cpp src/balltree/%.h
	mkdir -p $(BUILD_DIR)balltree
	$(CXX) -c $< $(CXX_FLAGS) $@

$(BUILD_DIR)csvparse/%.o: src/csvparse/%.cpp src/csvparse/%.h
	mkdir -p $(BUILD_DIR)csvparse
	$(CXX) -c $< $(CXX_FLAGS) $@

clean:
	rm -rf $(BUILD_DIR)

test: build
	mkdir -p build
	$(CXX) $(CPP_TEST_FILES) $(CPP_FILES) $(CXX_FLAGS) build/tests
	build/tests