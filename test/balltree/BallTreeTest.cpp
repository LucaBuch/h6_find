#include "../../lib/catch.hpp"
#include "../../src/balltree/BallTree.h"
#include "../../src/balltree/Point.h"

using namespace ball_tree;

TEST_CASE("BallTree simple constructor")
{
    Point p(1, 1);
    BallTree b(p);
    REQUIRE(b.getPoint().getX() == 1);
    REQUIRE(b.getPoint().getY() == 1);
}

TEST_CASE("BallTree complex constructor")
{
    Point p_1(1,1);
    Point p_2(2,2);
    Point p_3(3,3);
    BallTree* b_1 = new BallTree(p_1);
    BallTree* b_2 = new BallTree(p_2);
    BallTree* b_3 = new BallTree(p_3,b_1,b_2);    
    REQUIRE(b_3->getChildren().first == b_1);
    REQUIRE(b_3->getChildren().second == b_2);
    REQUIRE(b_3->getChildren().second->getPoint().getX() == 2);
    delete b_3;
}