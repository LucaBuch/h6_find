#include "../../lib/catch.hpp"
#include "../../src/balltree/BallTree.h"
#include "../../src/balltree/BallTreeBuilder.h"
#include "../../src/balltree/Point.h"
#include <iostream>

using namespace ball_tree;

TEST_CASE("BallTreeBuilder simple BallTree construction")
{
    Point p1(1, 1), p2(2, 2), p3(0, 1);
    std::vector<Point> points = {p1, p2, p3};
    BallTree *bt = BallTreeBuilder::constructBallTree(points);
    REQUIRE(bt->getPoint().getX() == 1);
}

TEST_CASE("BallTreeBuilder more complex BallTree construction")
{
    Point p1(-2, -2), p2(1, -5), p3(2, -2), p4(4, -3), p5(3, 2), p6(5, 6), p7(-1, 4), p8(-3, 2), p9(1, 2);
    std::vector<Point> points = {p1, p2, p3, p4, p5, p6, p7, p8, p9};
    BallTree* bt = BallTreeBuilder::constructBallTree(points);
    std::cout << bt->print();
    delete bt;
}